# Mobile app developed using react native for a school project

Functionalities:
	* Create an account
	* Log in using the created account
	* Create a chart with the cars from your dream collection
	* Add a car to your collection
	* Remove a car from your collection
	* Edit a car from your collection
	* See you collection and details about each car in it
	* Generate a .pdf file with you collection and sending it to your e-mail address
